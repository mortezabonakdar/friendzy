
# Friendzy API

The Friendzy is an application for find and search people and adding them as friend.

The API is implemented using Node.js (Typescript) and [NestJs](https://nestjs.com/) framework. Postgres database is used for data storage and [Prisma](https://www.prisma.io/) is also used as ORM.

To implement this project, I tried to implement the principles of clean code and modular programming. Also, this project has been done with the TDD method and unit and E2E tests have been implemented.

In the following, you can check the structure of some APIs. However, for a full review of all APIs, you can take a look at the document prepared using the open API standard at [API-docs](https://gitlab.com/mortezabonakdar/friendzy/-/blob/main/docs/openapi.yml).


## API Reference

#### Register User

```http
  POST /api/v1/auth/register
```

| Parameter | Type     | Description                | Example |
| :-------- | :------- | :------------------------- |:--------|
| `email` | `string` | **Required** | mortezabonakdar@gmail.com |
|  `mobile`| `string` | **Required** | 989121234567 |
|   `first_name` | `string` |**Required** | Morteza |
|  `last_name` | `string` | **Required** | Bonakdar |
| `age` | `number`| **Required** | 25 |
| `password` | `string` | **Required** | t@1234 |
| `confirm_password`| `string` | **Required** | t@1234 |

#### Login with email or mobile

```http
  Post /api/v1/auth/login
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `username`      | `string` | **Required**. Email or Mobile |
| `password` | `string` | **Required**|


You can see the project documentation that includes API documents in [docs](https://gitlab.com/mortezabonakdar/friendzy/-/tree/main/docs)


## Environment Variables

To run this project, you will need to add the following environment variables to your `.env` file

`DATABASE_URL`
 `postgresql://{postgres_user}:{postgres_pass}@{postgres_host}:{postgres_port}/{db_name}?schema=public`

`JWT_SECRET`

`PORT`


## Deployment

project preparation using npm:

```bash
  npm install
  npx prisma generate
  npx prisma migrate deploy
  npm run build
  node ./dist/src/main.js
```
OR simply you can use these commands:

```bash
  npm install
  npm start
```
Also you can run the project using docker-compose with `docker-compose.yml` file

```bash
version: '3.8'
services:
  app:
    build: .
    ports:
      - "3000:3000"
    environment:
      - DATABASE_URL="postgresql://postgres:postgres@db:5432/friendzy_db?schema=public"
      - JWT_SECRET= "DjfhHfj7f24SfdhEg5dD4265f234ds86"
      - PORT=3000
    depends_on:
      - db
  db:
    image: postgres
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
      - POSTGRES_DB=friendzy_db
    volumes:
      - ./postgres-data:/var/lib/postgresql/data
  nginx:
    image: nginx
    ports:
      - "80:80"
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    depends_on:
      - app
```
That `nginx.conf` is like this:

```bash
events {}
http {
  upstream app {
    server app:3000;
  }
  server {
    listen 80;
    location / {
      proxy_pass http://app;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
  }
}

```
## Tech Stack

Node.js(Typescript), NestJs, Prisma, PostgreSQL
