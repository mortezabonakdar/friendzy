import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { RegisterDto } from './../src/auth/dto/register.dto';
import { LoginDto } from 'src/auth/dto/login.dto';

// E2E Tests

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });
  describe('AUTH Tests', () => {
    let registerData: RegisterDto
    it('Register Test', async () => {
      registerData = {
        first_name: 'james',
        last_name: 'cameron',
        age: 20,
        email: (Math.random() + 1).toString(36).substring(7) + '@gmail.com',
        mobile: String(Math.floor(Math.random() * 1000000000)),
        password: 'T@123',
        confirm_password: 'T@123'
      }
      const response = await request(app.getHttpServer())
        .post('/api/v1/auth/register').send(registerData)
      expect(response.status).toEqual(201)
      expect(response.body.user.email).toEqual(registerData.email)
      expect(response.body.user.mobile).toEqual(registerData.mobile)
    })

    it('Login Test', async () => {
      const loginData: LoginDto = {
        username: registerData.email,
        password: registerData.password
      }
      const response = await request(app.getHttpServer())
        .post('/api/v1/auth/login').send(loginData)
      expect(response.status).toBe(200)
      expect(response.body.user.email).toEqual(loginData.username)
    })
  })
  describe('Invitation Tests', () => {
    let register
    it('Send Invitation', async () => {
      register = await request(app.getHttpServer())
        .post('/api/v1/auth/register').send({
          first_name: 'james',
          last_name: 'cameron',
          age: 20,
          email: (Math.random() + 1).toString(36).substring(7) + '@gmail.com',
          mobile: String(Math.floor(Math.random() * 1000000000)),
          password: 'T@123',
          confirm_password: 'T@123'
        })

      const sendInvitation = await request(app.getHttpServer())
        .post('/api/v1/invitations/send')
        .set('Authorization', 'Bearer ' + register.body.token)
        .send({
          invited_id: 2
        })
      expect(sendInvitation.status).toBe(200)
    })
    it('check invitation', async () => {
      const indexInvitation = await request(app.getHttpServer())
        .get('/api/v1/invitations')
        .set('Authorization', 'Bearer ' + register.body.token)
      expect(indexInvitation.status).toBe(200)

      const checkInvitation = await request(app.getHttpServer())
        .post(`${indexInvitation[0].id}`)
        .set('Authorization', 'Bearer ' + register.body.token)
      expect(checkInvitation.status).toBe(200)
    })

  })
  describe('Search Users Tests', () => {
    it('search users', async () => {
      const search = await request(app.getHttpServer())
        .get('/api/v1/users')
        .field('first_name', 'ali')
      expect(search.status).toBe(200)
    })
  })
});
