import { PrismaClient, User } from '@prisma/client'
const prisma = new PrismaClient()
const users_data: User[] = require('./seed-data/users-data.json')
import * as bcrypt from 'bcrypt'

async function main() {
  for (let user of users_data) {
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(user.password, salt)
    await prisma.user.create({
      data: {
        firstName: user.firstName,
        lastName: user.lastName,
        age: user.age,
        email: user.email,
        mobile: user.mobile,
        password: hash
      }
    })
  }
}
main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })