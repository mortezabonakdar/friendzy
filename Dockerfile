FROM node:16.14.2-alpine as builder

RUN mkdir -p /usr/src/app
RUN chmod 777 /usr/src/app
WORKDIR /usr/src/app


RUN npm config set legacy-peer-deps true
COPY package*.json ./
RUN npm ci
COPY . .


RUN npm run build



FROM node:16.14.2-alpine

ENV NODE_ENV production

RUN mkdir -p /usr/src/app
RUN chmod 777 /usr/src/app

USER node

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm ci --production

COPY --from=builder /usr/src/app/dist ./dist

EXPOSE 3000

CMD ["npm","start"]