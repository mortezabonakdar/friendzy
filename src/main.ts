import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import { HttpExceptionFilter } from './utils/filters/http-exception.filter';
import prisma from './prisma.service'
import { ConfigService } from '@nestjs/config';
import { IEnvironmentVariables } from './utils/interfaces';

async function bootstrap() {
  const logger = new Logger('app')
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
    disableErrorMessages: false,
    transform: true,
    whitelist: true
  }))
  app.useGlobalFilters(new HttpExceptionFilter)
  const configService = app.get<ConfigService<IEnvironmentVariables>>(ConfigService)
  await prisma.enableShutdownHooks(app)
  await app.listen(configService.get('PORT'), '0.0.0.0');
  logger.log(`Running app on port ${configService.get('PORT')}`)

}
bootstrap();
