export const selectWithoutPassword = () => {
    return {
        id: true,
        email: true,
        mobile: true,
        firstName: true,
        lastName: true,
        age: true,
        createdAt: true,
        updatedAt: true
    }
}