import { IHttpResponse } from './interfaces'

export default class Response {
  static okWithData(data: object): IHttpResponse {
    return {
      statusCode: 200,
      data: data
    }
  }
  static createdWithData(data: object): IHttpResponse {
    return {
      statusCode: 201,
      data: data
    }
  }
  static badRequestWithData({ code, message, status = 400 }): IHttpResponse {
    return {
      statusCode: status,
      data: {
        message,
        code
      }
    }
  }
  static deleted: IHttpResponse = this.okWithData({
    code: "Deleted",
    message: "Successfully deleted",
    status: 200
  })

  static updated: IHttpResponse = this.okWithData({
    code: 'Updated',
    message: 'Successfully updated',
    status: 200
  })

  static userNotFound: IHttpResponse = this.badRequestWithData({
    code: "UserNotFound",
    message: "User not found!",
    status: 400
  })

  static accessDenied: IHttpResponse = this.badRequestWithData({
    code: 'AccessDenied',
    message: 'you have not access to this page!',
    status: 403
  })

  static passwordNotMatch: IHttpResponse = this.badRequestWithData({
    code: 'PasswordNotMatch',
    message: 'The password is not match',
    status: 400
  })
}