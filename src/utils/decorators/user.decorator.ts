import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { IUser } from '../../utils/interfaces';

// Extract User object from request
export const User = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const user: IUser = ctx.switchToHttp().getRequest()['user']
    return user
  }
)
