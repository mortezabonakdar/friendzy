export interface IEnvironmentVariables {
  NODE_ENV: string,
  PORT: string,
  HOST: string,
  DB_HOST: string,
  DB_PORT: string,
  DB_USER: string,
  DB_PASS: string,
  DB_NAME: string,
  JWT_SECRET: string
}
export interface IHttpResponse {
  data: object,
  statusCode: number
}
export interface IUser {
  userId: number,
}