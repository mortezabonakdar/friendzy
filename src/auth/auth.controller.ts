import { Body, Controller, Post, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';
import { Response } from 'express';
import { LoginDto } from './dto/login.dto';

@Controller('api/v1/auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService
  ) { }

  @Post('/register')
  async register(
    @Body() registerDto: RegisterDto,
    @Res() response: Response
  ) {
    const { statusCode, data } = await this.authService.register(
      registerDto
      )
    response.status(statusCode).json(data)
  }

  @Post('/login')
  async login(
    @Body() loginDto: LoginDto,
    @Res() response: Response
  ){
    const {statusCode, data} = await this.authService.login(
      loginDto
      )
    response.status(statusCode).json(data)
  }
}
