import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import prisma from '../prisma.service';
import { IHttpResponse } from '../utils/interfaces';
import { RegisterDto } from './dto/register.dto';
import Response from '../utils/responses';
import { LoginDto } from './dto/login.dto';
import * as bcrypt from 'bcrypt'
import { User } from '@prisma/client';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
  ) { }

  async register(registerData: RegisterDto): Promise<IHttpResponse> {
    let user = await prisma.user.findFirst({
      where: {
        OR: [
          {
            email: registerData.email
          },
          {
            mobile: registerData.mobile
          }
        ]
      }
    })
    if (user) {
      return Response.badRequestWithData({
        code: 'UserAlreadyExist',
        message: 'The user is already exist.'
      })
    }
    if (registerData.password !== registerData.confirm_password) {
      return Response.passwordNotMatch
    }
    user = await prisma.user.create({
      data: {
        email: registerData.email,
        mobile: registerData.mobile,
        age: registerData.age,
        firstName: registerData.first_name,
        lastName: registerData.last_name,
        password: registerData.password
      }
    })
    return Response.createdWithData({
      token: this._generateToken(user),
      user: this._passwordSeparator(user)
    })
  }

  async login(loginData: LoginDto): Promise<IHttpResponse> {
    const user = await prisma.user.findFirst({
      where: {
        OR: [
          {
            email: loginData.username
          },
          {
            mobile: loginData.username
          }
        ]
      }
    })
    if (!user || !await bcrypt.compare(loginData.password, user.password)) {
      return Response.badRequestWithData({
        code: 'InvalidCredentials',
        message: 'Username or Password is incorrect.'
      })
    }
    return Response.okWithData({
      token: this._generateToken(user),
      user: this._passwordSeparator(user)
    })
  }

  private _generateToken(user: Partial<User>): string {
    const payload = { userId: user.id }
    return this.jwtService.sign(payload)
  }
  // remove password from user object
  private _passwordSeparator(userObj: User): Partial<User> {
    const { password, ...result } = userObj
    return result
  }
}
