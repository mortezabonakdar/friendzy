import { IsEmail, IsMobilePhone, IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class RegisterDto {

  @IsNotEmpty()
  @IsEmail()
  email: string

  @IsNotEmpty()
  @IsMobilePhone()
  mobile: string

  @IsNotEmpty()
  @IsString()
  first_name: string

  @IsNotEmpty()
  @IsString()
  last_name: string

  @IsNotEmpty()
  @IsNumber()
  age: number

  @IsNotEmpty()
  @IsString()
  password: string

  @IsNotEmpty()
  @IsString()
  confirm_password: string
}