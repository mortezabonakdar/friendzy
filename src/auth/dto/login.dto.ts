import { IsNotEmpty, IsString } from 'class-validator'
import { IsUsername } from '../../utils/validators/username.validator'

export class LoginDto {

  @IsUsername({
    message: "username is not valid"
  })
  username: string

  @IsNotEmpty()
  @IsString()
  password: string
}