export default () => ({
  port: parseInt(process.env.PORT) || 3000,
  host: parseInt(process.env.HOST),
  db: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    name: process.env.DB_NAME
  },
  node_env: process.env.NODE_ENV || 'development',
  jwt_secret: process.env.JWT_SECRET,
  

})