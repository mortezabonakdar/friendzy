import { Injectable } from '@nestjs/common';
import prisma from '../prisma.service';
import { IHttpResponse } from '../utils/interfaces';
import UpdateProfileDto from './dto/update-profile.dto';
import { User } from '@prisma/client';
import Response from '../utils/responses';
import SearchDto from './dto/search.dto';
import UpdatePasswordDto from './dto/update-password.dto';
import * as bcrypt from 'bcrypt'
import { selectWithoutPassword } from '../utils/const'

@Injectable()
export class UsersService {
  async updateProfile(user_id: number, profileData: UpdateProfileDto): Promise<IHttpResponse> {
    const user = await this.getUserById(user_id)
    if (!user) return Response.userNotFound
    await prisma.user.update({
      where: {
        id: user_id
      },
      data: profileData
    })
    return Response.updated
  }
  async updatePassword(user_id: number, updatePass: UpdatePasswordDto): Promise<IHttpResponse> {
    const user = await this.getUserById(user_id)
    if (!user) return Response.userNotFound
    if (updatePass.password !== updatePass.confirm_password) {
      return Response.passwordNotMatch
    }
    const salt = bcrypt.genSaltSync(10)
    const hash = bcrypt.hashSync(updatePass.password, salt)
    await prisma.user.update({
      where: {
        id: user_id
      },
      data: {
        password: hash
      }
    })
    return Response.okWithData({
      message: 'Password updated successfully.'
    })
  }
  async search(searchData: SearchDto): Promise<IHttpResponse> {
    Object.keys(searchData).forEach(key => {
      if (searchData[key] == null || searchData[key] == undefined || Number.isNaN(searchData[key])) {
        delete searchData[key]
      }
    })
    let users: Partial<User>[]
    if (Object.keys(searchData).length === 0) {
      users = await prisma.user.findMany()
    } else {
      users = await prisma.user.findMany({
        where: {
          OR: [
            {
              firstName: {
                contains: searchData.first_name ? searchData.first_name : undefined
              }
            },
            {
              lastName: {
                contains: searchData.last_name ? searchData.last_name : undefined
              }
            },
            {
              age: searchData.age ? Number(searchData.age) : undefined
            }
          ]
        },
        select: {
          ...selectWithoutPassword() // select all user fields except password
        }
      })
    }
    return Response.okWithData(users)
  }

  async getUserById(id: number): Promise<User | false> {
    const user = await prisma.user.findUnique({
      where: {
        id: id
      }
    })
    if (!user) return false
    return user
  }

}
