import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { FriendsModule } from './friends/friends.module';
import { InvitationsModule } from './friends/invitations/invitations.module';
import { UsersController } from './users.controller';

@Module({
  providers: [UsersService],
  imports: [FriendsModule, InvitationsModule],
  controllers: [UsersController],
})
export class UsersModule {}
