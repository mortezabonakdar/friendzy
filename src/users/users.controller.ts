import { Body, Controller, Get, Post, Put, Query, Res } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from '../utils/decorators/user.decorator';
import { IUser } from '../utils/interfaces';
import UpdateProfileDto from './dto/update-profile.dto';
import { Response } from 'express';
import UpdatePasswordDto from './dto/update-password.dto';

@Controller('api/v1/users')
export class UsersController {
  constructor(
    private usersService: UsersService
  ) { }

  @Put('/')
  async updateProfile(
    @User() user: IUser,
    @Body() updateProfileDto: UpdateProfileDto,
    @Res() response: Response
  ) {
    const { statusCode, data } = await this.usersService.updateProfile(
      user.userId,
      updateProfileDto
    )
    response.status(statusCode).json(data)
  }

  @Post('/reset-password')
  async updatePassword(
    @User() user: IUser,
    @Body() updatePasswordDto: UpdatePasswordDto,
    @Res() response: Response
  ) {
    const { statusCode, data } = await this.usersService.updatePassword(
      user.userId,
      updatePasswordDto
    )
    response.status(statusCode).json(data)
  }

  @Get('/')
  async search(
    @Query('first_name') first_name: string,
    @Query('last_name') last_name: string,
    @Query('age') age: number,
    @Res() response: Response
  ) {
    const searchData = { first_name, last_name, age }
    const { statusCode, data } = await this.usersService.search(
      searchData
    )
    response.status(statusCode).json(data)
  }
}
