import { Injectable } from '@nestjs/common';
import prisma from '../../prisma.service';
import { IHttpResponse } from '../../utils/interfaces';
import Response from '../../utils/responses';
import { UsersService } from '../users.service';
import { FriendList } from '@prisma/client';

@Injectable()
export class FriendsService {
  constructor(
    private userService: UsersService
  ) { }

  async index(user_id: number): Promise<IHttpResponse> {
    const user_friends = await prisma.friendList.findMany({
      where: {
        userId: user_id
      }
    })
    return Response.okWithData(user_friends)
  }
  async remove(user_id: number, friend_id: number): Promise<IHttpResponse> {
    const [user, friend] = await Promise.all([this.userService.getUserById(user_id), this.userService.getUserById(friend_id)])
    if (!user || !friend) return Response.userNotFound
    const user_friend = await prisma.friendList.findFirst({
      where: {
        userId: user_id,
        friendId: friend_id
      }
    })
    if (user_friend) {
      await prisma.friendList.delete({
        where: {
          id: user_friend.id
        }
      })
    }
    return Response.deleted
  }
  async add(user_id: number, friend_id: number): Promise<FriendList> {
    let user_friend = await prisma.friendList.findFirst({
      where: {
        AND: {
          userId: user_id,
          friendId: friend_id
        }
      }
    })
    if (!user_friend) {
      user_friend = await prisma.friendList.create({
        data: {
          userId: user_id,
          friendId: friend_id
        }
      })
    }
    return user_friend
  }
}
