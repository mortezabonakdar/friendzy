import { Controller, Delete, Get, Param, Res, UseGuards } from '@nestjs/common';
import { FriendsService } from './friends.service';
import { AuthGuard } from '../../auth/auth.guard'
import { User } from '../../utils/decorators/user.decorator';
import { IUser } from '../../utils/interfaces';
import { Response } from 'express';

@Controller('api/v1/users/friends')
export class FriendsController {
  constructor(
    private friendsService: FriendsService
  ) { }

  @UseGuards(AuthGuard)
  @Get('/')
  async index(
    @User() user: IUser,
    @Res() response: Response
  ) {
    const { statusCode, data } = await this.friendsService.index(
      user.userId
    )
    response.status(statusCode).json(data)
  }

  @UseGuards(AuthGuard)
  @Delete('/:user_id')
  async remove(
    @User() user: IUser,
    @Param('user_id') friend_id: number,
    @Res() response: Response
  ) {
    const { statusCode, data } = await this.friendsService.remove(
      user.userId,
      friend_id
    )
    response.status(statusCode).json(data)
  }
}
