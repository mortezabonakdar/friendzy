import { Body, Controller, Get, Param, Post, Res, UseGuards } from '@nestjs/common';
import { InvitationsService } from './invitations.service';
import { AuthGuard } from '../../../auth/auth.guard';
import { User } from '../../../utils/decorators/user.decorator';
import { IUser } from '../../../utils/interfaces';
import { Response } from 'express';
import CheckInvitationDto from './dto/check.dto';
import InviteDto from './dto/invite.dto';

@Controller('api/v1/invitations')
export class InvitationsController {
  constructor(
    private invitationsService: InvitationsService
  ) { }

  @UseGuards(AuthGuard)
  @Get('/')
  async index(
    @User() user: IUser,
    @Res() response: Response
  ) {
    const { statusCode, data } = await this.invitationsService.index(
      user.userId
    )
    response.status(statusCode).json(data)
  }

  @UseGuards(AuthGuard)
  @Post('/send')
  async send(
    @User() user: IUser,
    @Body() sendInvitationDto: InviteDto,
    @Res() response: Response
  ) {
    const { statusCode, data } = await this.invitationsService.send(
      user.userId,
      sendInvitationDto.invited_id
    )
    response.status(statusCode).json(data)
  }
  
  @UseGuards(AuthGuard)
  @Post('/:id')
  async check(
    @User() user: IUser,
    @Param('id') invitation_id: number,
    @Body() checkDto: CheckInvitationDto,
    @Res() response: Response
  ) {
    const { statusCode, data } = await this.invitationsService.check(
      user.userId,
      invitation_id,
      checkDto.is_accepted
    )
    response.status(statusCode).json(data)
  }


}
