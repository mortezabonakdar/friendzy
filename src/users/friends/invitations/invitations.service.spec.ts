import { Test, TestingModule } from '@nestjs/testing';
import { FriendsService } from '../friends.service';
import prisma from '../../../prisma.service'
import { User } from '@prisma/client';
import { UsersService } from '../../users.service';
import { InvitationsService } from './invitations.service';
import { IHttpResponse } from 'src/utils/interfaces';

describe('InvitationsTest', () => {
  let service: InvitationsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [InvitationsService, UsersService, FriendsService],
    }).compile();

    service = module.get<InvitationsService>(InvitationsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  let user: User
  let invitedUser: User
  it('Send invitations test', async () => {
    user = await prisma.user.findFirst({
      where: {
        firstName: 'test1'
      }
    })
    invitedUser = await prisma.user.findFirst({
      where:{
        firstName: 'test5'
      }
    })
    const sendInvite: any = await service.send(user.id, invitedUser.id)
    expect(sendInvite.statusCode).toBe(200)
    expect(sendInvite.data.invitedId).toEqual(invitedUser.id)
    expect(sendInvite.data.applicantId).toEqual(user.id)
  })

  it('Check specific invitation', async () => {
    const indexInvitation = await service.index(invitedUser.id)
    expect(indexInvitation.statusCode).toBe(200)
    expect(indexInvitation.data).not.toHaveLength(0)
    expect(indexInvitation.data[0].applicantId).toEqual(user.id)
    expect(indexInvitation.data[0].invitedId).toEqual(invitedUser.id)

    const checkInvitation = await service.check(invitedUser.id, indexInvitation.data[0].id, true)
    expect(checkInvitation.statusCode).toBe(200)
    
    const acceptedInvite = await prisma.invitation.findFirst({
      where:{
        invitedId: invitedUser.id,
        applicantId: user.id
      }
    })
    expect(acceptedInvite.isChecked).toBe(true)
  })


});