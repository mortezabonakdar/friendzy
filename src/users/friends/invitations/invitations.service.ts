import { Injectable } from '@nestjs/common';
import prisma from '../../../prisma.service';
import { IHttpResponse } from '../../../utils/interfaces';
import Response from '../../../utils/responses';
import { FriendsService } from '../friends.service';

@Injectable()
export class InvitationsService {
  constructor(
    private friendsService: FriendsService
  ) { }

  async index(user_id: number): Promise<IHttpResponse> {
    const invitations = await prisma.invitation.findMany({
      where: {
        AND: {
          invitedId: user_id,
          isChecked: false
        }
      }
    })
    return Response.okWithData(invitations)
  }

  async check(user_id: number, invitation_id: number, is_accepted: boolean): Promise<IHttpResponse> {
    const invitation = await prisma.invitation.findFirst({
      where: {
        AND: {
          id: invitation_id,
          invitedId: user_id
        }
      }
    })
    if (!invitation) {
      return Response.badRequestWithData({
        code: 'InvitationNotFound',
        message: 'Invitation not found',
        status: 400
      })
    }
    await prisma.invitation.update({
      where: {
        id: invitation.id
      },
      data: {
        isChecked: true
      }
    })
    let message: string = 'The user was rejected to be added to the friends list'
    if (is_accepted) {
      await this.friendsService.add(user_id, invitation.applicantId)
      message = 'user added to friend list successfully.'
    }
    return Response.okWithData({
      message: message
    })

  }

  async send(user_id: number, invited_id: number): Promise<IHttpResponse> {
    let invitation = await prisma.invitation.findFirst({
      where: {
        AND: {
          applicantId: user_id,
          invitedId: invited_id,
        }
      }
    })
    if (invitation) {
      return Response.badRequestWithData({
        code: 'InvitationExist',
        message: 'The invitation is already exist.'
      })
    }
    invitation = await prisma.invitation.create({
      data: {
        applicantId: user_id,
        invitedId: invited_id,
        isChecked: false
      }
    })
    return Response.okWithData(invitation)
  }
}
