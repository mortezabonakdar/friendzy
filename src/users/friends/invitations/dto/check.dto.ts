import { IsBoolean, IsNotEmpty } from 'class-validator'

export default class CheckInvitationDto {
  
  @IsBoolean()
  @IsNotEmpty()
  is_accepted: boolean
}