import { IsNotEmpty, IsNumber } from "class-validator";

export default class InviteDto {

  @IsNumber()
  @IsNotEmpty()
  invited_id: number
}