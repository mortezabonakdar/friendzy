import { Module } from '@nestjs/common';
import { InvitationsService } from './invitations.service';
import { InvitationsController } from './invitations.controller';
import { FriendsService } from '../friends.service';
import { UsersService } from '../../users.service';

@Module({
  providers: [InvitationsService, FriendsService, UsersService],
  controllers: [InvitationsController]
})
export class InvitationsModule {}
