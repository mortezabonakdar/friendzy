import { Test, TestingModule } from '@nestjs/testing';
import { FriendsService } from './friends.service';
import prisma from '../../prisma.service'
import { User } from '@prisma/client';
import { UsersService } from '../users.service';

describe('FreindService', () => {
  let service: FriendsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FriendsService, UsersService],
    }).compile();

    service = module.get<FriendsService>(FriendsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  let user: User
  let friend: User
  it('add friend Test', async () => {
    user = await prisma.user.findFirst({
      where: {
        firstName: 'test3'
      }
    })
    friend = await prisma.user.findFirst({
      where:{
        firstName: 'test4'
      }
    })
    const addFriend = await service.add(user.id, friend.id)
    expect(addFriend.friendId).toEqual(friend.id)
    expect(addFriend.userId).toEqual(user.id)
  })

  it('index friends Test', async () => {
    const friends = await service.index(user.id)
    expect(friends.statusCode).toBe(200)
    expect(friends.data).not.toHaveLength(0)
    expect(friends.data[0].userId).toEqual(user.id)
  })



});