import { Module } from '@nestjs/common';
import { FriendsService } from './friends.service';
import { FriendsController } from './friends.controller';
import { UsersService } from '../users.service';
import { UsersModule } from '../users.module';

@Module({
  providers: [FriendsService, UsersService],
  controllers: [FriendsController],
})
export class FriendsModule {
}
