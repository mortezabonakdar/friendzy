import { IsNotEmpty, IsString } from 'class-validator'

export default class UpdatePasswordDto {

  @IsString()
  @IsNotEmpty()
  password: string

  @IsString()
  @IsNotEmpty()
  confirm_password: string
}