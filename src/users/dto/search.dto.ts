import { IsNumber, IsString } from "class-validator"

export default class SearchDto {
  
  @IsString()
  first_name: string

  @IsString()
  last_name: string

  @IsNumber()
  age: number
}