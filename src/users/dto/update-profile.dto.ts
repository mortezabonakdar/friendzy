import { IsEmail, IsMobilePhone, IsString, IsNumber } from 'class-validator'

export default class UpdateProfileDto {
  @IsEmail()
  email: string

  @IsMobilePhone()
  mobile: string

  @IsString()
  first_name: string

  @IsString()
  last_name: string

  @IsNumber()
  age: number
}