import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import prisma from '../prisma.service'

describe('UsersService', () => {
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService]
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  it('Update profile Tests', async () => {
    const user = await prisma.user.findFirst({
      where:{
        firstName: 'test2'
      }
    })
    const updateProfileData: any = {
      firstName: 'ali',
      lastName: 'alavi',
      age: 30,
    }
    const updateProfile = await service.updateProfile(user.id, updateProfileData)
    expect(updateProfile.statusCode).toBe(200)
    const userAfterUpdate = await prisma.user.findFirst({
      where: {
        id: user.id
      }
    })
    expect(userAfterUpdate.firstName).toEqual(updateProfileData.firstName)
    expect(userAfterUpdate.lastName).toEqual(updateProfileData.lastName)
    expect(userAfterUpdate.age).toEqual(updateProfileData.age)
  })
  it('Search users Tests', async () => {
    const searchData: any = {
      first_name: 'test1',
      age: 22
    }
    const searchUsers = await service.search(searchData)
    expect(searchUsers.statusCode).toBe(200)
    expect(searchUsers.data).toHaveLength(1)
  })
});
