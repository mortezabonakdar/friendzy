                    +-----------------+
                    |    AppModule    |
                    |-----------------|
                    |      auth       |
                    |     config      |
                    |     users       |
                    |     utils       |
                    +--------+--------+
                             |
                             |
                    +--------v--------+
                    |   UsersModule   |
                    |-----------------|
                    |     friends     |
                    +--------+--------+
                             |
                             |
                    +--------v--------+
                    |  FriendsModule  |
                    |-----------------|
                    |   invitation    |
                    +-----------------+
