                                    +--------------+
                                    |     User     |
                                    +--------------+
                                    | id           |
                                    | email        |
                                    | mobile       |
                                    | first_name   |
                                    | last_name    |
                                    | age          |
                                    | password     |
                                    | created_at   |
                                    | updated_at   |
                                    +--------------+
                                             |
                                             |
                        +--------------------+--------------------+
                        |                                         |
                        |                                         |
               +--------------+                         +--------------+
               |  Invitation  |                         |  FriendList  |
               +--------------+                         +--------------+
               | id           |                         | id           |
               | applicant_id |                         | user_id      |
               | invited_id   |                         | friend_id    |
               | is_checked   |                         +--------------+
               | created_at   |
               | updated_at   |
               +--------------+
                     |                                        |
                     |                                        |
           +----------------------+                 +----------------------+
           |                      |                 |                      |
   +------------------+   +-----------------+   +------------------+   +-----------------+
   |  User (invitedUser) |   |  User (applicantUser) |   |  User (friend)   |   |  User (owner)   |
   +------------------+   +-----------------+   +------------------+   +-----------------+
   | id               |   | id              |   | id               |   | id              |
   | email            |   | email           |   | email            |   | email           |
   | mobile           |   | mobile          |   | mobile           |   | mobile          |
   | first_name       |   | first_name      |   | first_name       |   | first_name      |
   | last_name        |   | last_name       |   | last_name        |   | last_name       |
   | age              |   | age             |   | age              |   | age             |
   | password         |   | password        |   | password         |   | password        |
   | created_at       |   | created_at      |   | created_at       |   | created_at      |
   | updated_at       |   | updated_at      |   | updated_at       |   | updated_at      |
   | postedInvitations |   | recievedInvitations |   | friendList1      |   | friendList2      |
   | recievedInvitations |   | postedInvitations   |   | friendList2      |   | friendList1      |
   +------------------+   +-----------------+   +------------------+   +-----------------+

This diagram shows that there are three tables in the database: 
User, Invitation, and FriendList. 
The User table has relationships with both the Invitation and FriendList tables. 
The Invitation table has a relationship with the User table through two foreign keys, applicant_id and invited_id, while the FriendList table has a relationship with the User table through two foreign keys, user_id and friend_id. 
The relationships between tables are represented by arrows pointing from the foreign key column to the primary key column of the related table.



